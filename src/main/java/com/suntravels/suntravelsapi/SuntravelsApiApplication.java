package com.suntravels.suntravelsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author Yoshani Ranaweera
 */

@SpringBootApplication
public class SuntravelsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuntravelsApiApplication.class, args);
	}

}
