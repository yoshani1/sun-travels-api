package com.suntravels.suntravelsapi.controller;

import com.suntravels.suntravelsapi.controller.mapper.ContractMapper;
import com.suntravels.suntravelsapi.controller.mapper.ContractRoomTypeMapper;
import com.suntravels.suntravelsapi.controller.viewmodel.*;
import com.suntravels.suntravelsapi.entity.Contract;
import com.suntravels.suntravelsapi.entity.ContractRoomType;
import com.suntravels.suntravelsapi.entity.Room;
import com.suntravels.suntravelsapi.entity.RoomType;
import com.suntravels.suntravelsapi.repository.ContractRepository;
import com.suntravels.suntravelsapi.repository.ContractRoomTypeRepository;
import com.suntravels.suntravelsapi.repository.RoomRepository;
import com.suntravels.suntravelsapi.repository.RoomTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
public class ContractController {

    @Autowired private ContractRepository contractRepository;
    @Autowired private ContractMapper contractMapper;
    @Autowired private ContractRoomTypeRepository contractRoomTypeRepository;
    @Autowired private ContractRoomTypeMapper contractRoomTypeMapper;
    @Autowired private RoomTypeRepository roomTypeRepository;
    @Autowired private RoomRepository roomRepository;


    @PostMapping("/addContract")
    public void save(@RequestBody AddContractDTO addContractDTO, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new ValidationException();
        }

        ContractViewModel dto = addContractDTO.getContractViewModel();
        List<ContractRoomTypeViewModel> crtDto = addContractDTO.getContractRoomTypeViewModelList();

        //save contract
        Contract contract = contractMapper.dtoToEntity(dto);
        UUID contractId = UUID.randomUUID();
        contract.setId(contractId);
        contractRepository.save(contract);

        //save list of contract room types
        for (ContractRoomTypeViewModel crt : crtDto){
            crt.setContractId(contractId);

            //implementation to set the availability to true of any number of rooms per room type that will be offered under current contract

            RoomType roomType = roomTypeRepository.findById(crt.getRoomTypeId())
                    .orElseThrow(() -> new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Room type not found"
            ));
            List<Room> roomsPerRoomType = roomRepository.findByRoomType(roomType);
            //when adding a new contract all rooms' availability will be set to false
            for (Room room: roomsPerRoomType){
                room.setAvailability(false);
            }
            //rooms per room type will be set to true according to the numbers given by new contract
            for (int j=0; j<crt.getNoOfRooms(); j++){
                roomsPerRoomType.get(j).setAvailability(true);
            }
            roomRepository.saveAll(roomsPerRoomType);
            }

        List<ContractRoomType> contractRoomTypeList = contractRoomTypeMapper.convertToContractRoomType(crtDto);

        contractRoomTypeRepository.saveAll(contractRoomTypeList);

    }

// get all contracts in accordance with template
    @GetMapping("/allContracts")
    public List<ViewContractDTO> findAllContractViews() {
        List<ViewContractDTO> viewContractDTOList = new ArrayList<>();
        List<Contract> contracts = contractRepository.findAllByOrderByEndDateDesc();
        for (Contract contract: contracts){

            ViewContractDTO viewContractDTO = new ViewContractDTO();

            viewContractDTO.setHotelName(contract.getHotel().getHotelName());
            viewContractDTO.setHotelAddress(contract.getHotel().getAddress());
            viewContractDTO.setStartDate(contract.getStartDate());
            viewContractDTO.setEndDate(contract.getEndDate());

            List<ContractRoomEntryDTO> contractRoomEntryDTOList = new ArrayList<>();
            List<ContractRoomType> contractRoomTypeList = contract.getContractRoomTypeList();

            for (ContractRoomType contractRoomType: contractRoomTypeList){

                ContractRoomEntryDTO contractRoomEntryDTO = new ContractRoomEntryDTO();

                contractRoomEntryDTO.setRoomType(contractRoomType.getRoomType().getType());
                contractRoomEntryDTO.setPrice(contractRoomType.getPrice());
                contractRoomEntryDTO.setNoOfRooms(contractRoomType.getNoOfRooms());
                contractRoomEntryDTO.setMaxAdults(contractRoomType.getRoomType().getMaxAdults());

                contractRoomEntryDTOList.add(contractRoomEntryDTO);
            }

            viewContractDTO.setContractRoomEntryDTOList(contractRoomEntryDTOList);

            viewContractDTOList.add(viewContractDTO);
        }

        return viewContractDTOList;
    }

}
