package com.suntravels.suntravelsapi.controller;

import com.suntravels.suntravelsapi.controller.mapper.HotelMapper;
import com.suntravels.suntravelsapi.controller.mapper.RoomTypeMapper;
import com.suntravels.suntravelsapi.controller.viewmodel.*;
import com.suntravels.suntravelsapi.entity.Contract;
import com.suntravels.suntravelsapi.entity.Hotel;
import com.suntravels.suntravelsapi.entity.Room;
import com.suntravels.suntravelsapi.entity.RoomType;
import com.suntravels.suntravelsapi.repository.ContractRepository;
import com.suntravels.suntravelsapi.repository.HotelRepository;
import com.suntravels.suntravelsapi.repository.RoomRepository;
import com.suntravels.suntravelsapi.repository.RoomTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
public class HotelController {

    @Autowired private HotelMapper hotelMapper;
    @Autowired private HotelRepository hotelRepository;
    @Autowired private RoomTypeRepository roomTypeRepository;
    @Autowired private RoomTypeMapper roomTypeMapper;
    @Autowired private ContractRepository contractRepository;
    @Autowired private RoomRepository roomRepository;

    // returns all hotels without valid contracts
    @GetMapping("/hotels")
    public List<HotelViewModel> findAllHotels() {
        Date todayDate = new Date();
        List<Hotel> hotels= hotelRepository.findAll();
        List<Hotel> hotelsWithoutValidContracts = new ArrayList<>();

        for (Hotel hotel: hotels){
            List<Contract> contracts = contractRepository.findByHotelId(hotel.getId());
            if (contracts.size()==0){
                hotelsWithoutValidContracts.add(hotel);
            }
            for (int i=0; i<contracts.size(); i++){
                if (todayDate.compareTo(contracts.get(i).getEndDate()) < 0){
                    break;
                }
                if (i==contracts.size()-1){
                    hotelsWithoutValidContracts.add(hotel);
                }
            }

        }
        return hotelMapper.entityToDto(hotelsWithoutValidContracts);
    }

    // save a hotel
    @PostMapping("/addHotel")
    public void save(@RequestBody AddHotelDTO addHotelDTO, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new ValidationException();
        }

        HotelViewModel dto = addHotelDTO.getHotelViewModel();
        List<RoomTypeViewModel> crtDto = addHotelDTO.getRoomTypeViewModelList();

        //save hotel
        Hotel hotel = hotelMapper.dtoToEntity(dto);
        UUID hotelId = UUID.randomUUID();
        hotel.setId(hotelId);
        hotelRepository.save(hotel);

        //save list of room types
        for (RoomTypeViewModel crt : crtDto){
            crt.setHotelId(hotelId);
            crt.setRoomTypeId(UUID.randomUUID());
        }

        List<RoomType> roomTypeList = roomTypeMapper.dtoToEntity(crtDto);

        roomTypeRepository.saveAll(roomTypeList);

        //create rooms per room type
        for (RoomType roomType: roomTypeList){
            List<Room> roomsPerRoomType = new ArrayList<>();
            for (int i=0; i<roomType.getNoOfRooms(); i++){
                Room room = new Room(
                        UUID.randomUUID(),
                        roomType,
                        false
                );
                roomsPerRoomType.add(room);
            }
            roomRepository.saveAll(roomsPerRoomType);
        }

    }
}
