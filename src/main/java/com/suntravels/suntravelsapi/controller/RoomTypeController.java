package com.suntravels.suntravelsapi.controller;

import com.suntravels.suntravelsapi.controller.mapper.RoomTypeMapper;
import com.suntravels.suntravelsapi.controller.viewmodel.RoomTypeViewModel;
import com.suntravels.suntravelsapi.entity.Hotel;
import com.suntravels.suntravelsapi.entity.RoomType;
import com.suntravels.suntravelsapi.repository.HotelRepository;
import com.suntravels.suntravelsapi.repository.RoomTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
public class RoomTypeController {

    @Autowired private RoomTypeRepository roomTypeRepository;
    @Autowired private HotelRepository hotelRepository;
    @Autowired RoomTypeMapper mapper;

    // get room types by hotel
    @GetMapping("/byHotel/{hotelId}")
    public List<RoomTypeViewModel> findRoomTypesByHotel(@PathVariable UUID hotelId){
        List<RoomType> roomTypes;

        Hotel hotel = this.hotelRepository.findById(hotelId)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, "Hotel not found"
                ));
        roomTypes = this.roomTypeRepository.findByHotel(hotel);
        return mapper.entityToDto(roomTypes);
    }

}
