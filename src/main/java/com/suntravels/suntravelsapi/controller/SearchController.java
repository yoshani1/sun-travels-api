package com.suntravels.suntravelsapi.controller;

import com.suntravels.suntravelsapi.controller.mapper.DateMapper;
import com.suntravels.suntravelsapi.controller.viewmodel.*;
import com.suntravels.suntravelsapi.entity.*;
import com.suntravels.suntravelsapi.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.ValidationException;
import java.util.*;

@RestController
@CrossOrigin
public class SearchController {

    @Autowired private HotelRepository hotelRepository;
    @Autowired private ContractRepository contractRepository;
    @Autowired private ContractRoomTypeRepository contractRoomTypeRepository;
    @Autowired private RoomTypeRepository roomTypeRepository;
    @Autowired private RoomRepository roomRepository;
    @Autowired private DateMapper dateMapper;

    @PostMapping("/searchRooms")
    public List<SearchResultDTO> roomSearch(@RequestBody SearchQueryDTO searchQueryDTO, BindingResult bindingResult) {

        List<RoomPrefDTO> roomPrefList = searchQueryDTO.getRoomPref();

        if (bindingResult.hasErrors()) {
            throw new ValidationException();
        }
        //final list of search results per hotel
        List<SearchResultDTO> searchResultDTOList = new ArrayList<>();
        Date todayDate = new Date();
        List<Hotel> hotels = hotelRepository.findAll();

        for (Hotel hotel: hotels){
            //only one valid(started and non expired) contract exists within period
            Date endDate = dateMapper.convertToDateViaSqlDate(dateMapper.convertToLocalDateViaSqlDate(searchQueryDTO.getStartDate()).plusDays(searchQueryDTO.getNoOfNights()));
            Contract validContract = contractRepository.findByHotelAndEndDateAfterAndStartDateBeforeAndEndDateAfter(hotel, todayDate, todayDate, endDate);

                List<SearchResultRoomEntryDTO> searchResultRoomEntryDTOList = new ArrayList<>();
                for (RoomPrefDTO roomPref: roomPrefList){
                    // get all contract room types which have equal number of adults to query
                    List<ContractRoomType> contractRoomTypeFilteredByNoOfAdultsList = contractRoomTypeRepository.findByContractAndRoomTypeMaxAdultsEquals(validContract, roomPref.getNoOfAdults());

                    List<SearchResultTypePriceDTO> searchResultTypePriceDTOList = new ArrayList<>();

                    // get all contract room types which have number of rooms that are available at present >= number of required rooms
                    for (ContractRoomType contractRoomType: contractRoomTypeFilteredByNoOfAdultsList){
                        // get number of rooms available
                        int noOfRoomsAvailable = roomRepository.findByRoomTypeAndAvailabilityTrue(contractRoomType.getRoomType()).size();
                        if (noOfRoomsAvailable >= roomPref.getNoOfRooms()){

                            // calculate marked up price per room (for requested number of nights)
                            double markedUpPrice = contractRoomType.getPrice()*(validContract.getMarkup()+100)*0.01*searchQueryDTO.getNoOfNights()*roomPref.getNoOfAdults();

                            searchResultTypePriceDTOList.add(new SearchResultTypePriceDTO(contractRoomType.getRoomType().getType(), markedUpPrice));
                        }
                    }

                    searchResultRoomEntryDTOList.add(new SearchResultRoomEntryDTO(roomPref, searchResultTypePriceDTOList));

                }
            if (validContract!=null){
                searchResultDTOList.add(new SearchResultDTO(hotel.getHotelName(), hotel.getAddress(), searchResultRoomEntryDTOList));
            }

        }
        return searchResultDTOList;
    }

}
