package com.suntravels.suntravelsapi.controller.mapper;

import com.suntravels.suntravelsapi.controller.viewmodel.ContractViewModel;
import com.suntravels.suntravelsapi.entity.Contract;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ContractMapper {
    public ContractViewModel entityToDto(Contract entity) {
        ModelMapper mapper =new ModelMapper();
        return mapper.map(entity, ContractViewModel.class);
    }

    public Contract dtoToEntity(ContractViewModel viewModel){
        ModelMapper mapper = new ModelMapper();
        return mapper.map(viewModel, Contract.class);
    }

    public List<ContractViewModel> entityToDto(List<Contract> entityList) {
        return	entityList.stream().map(this::entityToDto).collect(Collectors.toList());
    }

    public List<Contract> dtoToEntity(List<ContractViewModel> viewModelList) {
        return viewModelList.stream().map(this::dtoToEntity).collect(Collectors.toList());
    }
}
