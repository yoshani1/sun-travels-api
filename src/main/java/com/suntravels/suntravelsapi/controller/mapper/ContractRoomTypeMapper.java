package com.suntravels.suntravelsapi.controller.mapper;

import com.suntravels.suntravelsapi.controller.viewmodel.ContractRoomTypeViewModel;
import com.suntravels.suntravelsapi.entity.ContractRoomType;
import com.suntravels.suntravelsapi.entity.ContractRoomTypePK;
import com.suntravels.suntravelsapi.repository.ContractRepository;
import com.suntravels.suntravelsapi.repository.RoomTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ContractRoomTypeMapper {

    @Autowired private ContractRepository contractRepository;
    @Autowired private RoomTypeRepository roomTypeRepository;

    //convert contract room type view model to contract room type entity
    public ContractRoomType convertToContractRoomType(ContractRoomTypeViewModel contractRoomTypeViewModel){
        ContractRoomTypePK contractRoomTypePK = new ContractRoomTypePK(contractRoomTypeViewModel.getContractId(), contractRoomTypeViewModel.getRoomTypeId() );

        return new ContractRoomType(
                contractRoomTypePK,
                contractRoomTypeViewModel.getNoOfRooms(),
                contractRoomTypeViewModel.getPrice(),
                this.contractRepository.findById(contractRoomTypePK.getContractId()).get(),
                this.roomTypeRepository.findById(contractRoomTypePK.getRoomTypeId()).get()
        );
    }


    public List<ContractRoomType> convertToContractRoomType(List<ContractRoomTypeViewModel> viewModelList) {
        return viewModelList.stream().map(this::convertToContractRoomType).collect(Collectors.toList());
    }


}
