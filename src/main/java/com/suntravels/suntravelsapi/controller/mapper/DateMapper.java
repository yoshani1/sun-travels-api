package com.suntravels.suntravelsapi.controller.mapper;

import org.springframework.stereotype.Component;
import java.time.LocalDate;
import java.util.Date;

//converts between LocalDate and Date types
@Component
public class DateMapper {
    public LocalDate convertToLocalDateViaSqlDate(Date dateToConvert) {
        return new java.sql.Date(dateToConvert.getTime()).toLocalDate();
    }

    public Date convertToDateViaSqlDate(LocalDate dateToConvert) {
        return java.sql.Date.valueOf(dateToConvert);
    }
}
