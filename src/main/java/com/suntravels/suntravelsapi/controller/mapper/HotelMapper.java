package com.suntravels.suntravelsapi.controller.mapper;

import com.suntravels.suntravelsapi.controller.viewmodel.HotelViewModel;
import com.suntravels.suntravelsapi.entity.Hotel;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class HotelMapper {

    //convert hotel entity to hotel view model
    public HotelViewModel entityToDto(Hotel hotel) {
        ModelMapper mapper =new ModelMapper();
        return mapper.map(hotel, HotelViewModel.class);
    }

    //convert hotel view model to hotel entity
    public Hotel dtoToEntity(HotelViewModel hotelViewModel){
        ModelMapper mapper = new ModelMapper();
        return mapper.map(hotelViewModel, Hotel.class);
    }

    public  List<HotelViewModel> entityToDto(List<Hotel> hotel) {

        return	hotel.stream().map(this::entityToDto).collect(Collectors.toList());

    }

    public List<Hotel> dtoToEntity(List<HotelViewModel> dto)
    {

        return dto.stream().map(this::dtoToEntity).collect(Collectors.toList());
    }
}
