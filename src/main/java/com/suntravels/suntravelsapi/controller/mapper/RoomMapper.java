package com.suntravels.suntravelsapi.controller.mapper;

import com.suntravels.suntravelsapi.controller.viewmodel.RoomViewModel;
import com.suntravels.suntravelsapi.entity.Room;
import org.modelmapper.ModelMapper;
import java.util.List;
import java.util.stream.Collectors;

public class RoomMapper {

    public RoomViewModel entityToDto(Room room) {
        ModelMapper mapper =new ModelMapper();
        return mapper.map(room, RoomViewModel.class);
    }

    public Room dtoToEntity(RoomViewModel roomViewModel){
        ModelMapper mapper = new ModelMapper();
        return mapper.map(roomViewModel, Room.class);
    }

    public List<RoomViewModel> entityToDto(List<Room> room) {

        return	room.stream().map(this::entityToDto).collect(Collectors.toList());

    }

    public List<Room> dtoToEntity(List<RoomViewModel> dto)
    {

        return dto.stream().map(this::dtoToEntity).collect(Collectors.toList());
    }
}
