package com.suntravels.suntravelsapi.controller.mapper;

import com.suntravels.suntravelsapi.controller.viewmodel.RoomTypeViewModel;
import com.suntravels.suntravelsapi.entity.RoomType;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RoomTypeMapper {
    public RoomTypeViewModel entityToDto(RoomType entity) {
        ModelMapper mapper =new ModelMapper();
        return mapper.map(entity, RoomTypeViewModel.class);
    }

    public RoomType dtoToEntity(RoomTypeViewModel viewModel){
        ModelMapper mapper = new ModelMapper();
        return mapper.map(viewModel, RoomType.class);
    }

    public List<RoomTypeViewModel> entityToDto(List<RoomType> entityList) {
        return	entityList.stream().map(this::entityToDto).collect(Collectors.toList());
    }

    public List<RoomType> dtoToEntity(List<RoomTypeViewModel> viewModelList) {
        return viewModelList.stream().map(this::dtoToEntity).collect(Collectors.toList());
    }
}
