package com.suntravels.suntravelsapi.controller.viewmodel;

import java.util.List;

public class AddContractDTO {
    private ContractViewModel contractViewModel;
    private List<ContractRoomTypeViewModel> contractRoomTypeViewModelList;

    public ContractViewModel getContractViewModel() {
        return contractViewModel;
    }

    public void setContractViewModel(ContractViewModel contractViewModel) {
        this.contractViewModel = contractViewModel;
    }

    public List<ContractRoomTypeViewModel> getContractRoomTypeViewModelList() {
        return contractRoomTypeViewModelList;
    }

    public void setContractRoomTypeViewModelList(List<ContractRoomTypeViewModel> contractRoomTypeViewModelList) {
        this.contractRoomTypeViewModelList = contractRoomTypeViewModelList;
    }
}
