package com.suntravels.suntravelsapi.controller.viewmodel;

import java.util.List;

public class AddHotelDTO {
    private HotelViewModel hotelViewModel;
    private List<RoomTypeViewModel> roomTypeViewModelList;

    public AddHotelDTO() {
    }

    public AddHotelDTO(HotelViewModel hotelViewModel, List<RoomTypeViewModel> roomTypeViewModelList) {
        this.hotelViewModel = hotelViewModel;
        this.roomTypeViewModelList = roomTypeViewModelList;
    }

    public HotelViewModel getHotelViewModel() {
        return hotelViewModel;
    }

    public void setHotelViewModel(HotelViewModel hotelViewModel) {
        this.hotelViewModel = hotelViewModel;
    }

    public List<RoomTypeViewModel> getRoomTypeViewModelList() {
        return roomTypeViewModelList;
    }

    public void setRoomTypeViewModelList(List<RoomTypeViewModel> roomTypeViewModelList) {
        this.roomTypeViewModelList = roomTypeViewModelList;
    }
}
