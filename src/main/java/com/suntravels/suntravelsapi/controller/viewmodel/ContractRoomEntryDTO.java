package com.suntravels.suntravelsapi.controller.viewmodel;

public class ContractRoomEntryDTO {

    private String roomType;
    private double price;
    private int noOfRooms;
    private int maxAdults;


    public ContractRoomEntryDTO() {
    }

    public ContractRoomEntryDTO(String roomType, double price, int noOfRooms, int maxAdults) {
        this.roomType = roomType;
        this.price = price;
        this.noOfRooms = noOfRooms;
        this.maxAdults = maxAdults;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getNoOfRooms() {
        return noOfRooms;
    }

    public void setNoOfRooms(int noOfRooms) {
        this.noOfRooms = noOfRooms;
    }

    public int getMaxAdults() {
        return maxAdults;
    }

    public void setMaxAdults(int maxAdults) {
        this.maxAdults = maxAdults;
    }
}
