package com.suntravels.suntravelsapi.controller.viewmodel;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class ContractRoomTypeViewModel {

    private UUID contractId;

    private UUID roomTypeId;

    @NotNull
    private int noOfRooms;

    @NotNull
    private double price;


    public UUID getContractId() {
        return contractId;
    }

    public void setContractId(UUID contractId) {
        this.contractId = contractId;
    }

    public UUID getRoomTypeId() {
        return roomTypeId;
    }

    public void setRoomTypeId(UUID roomTypeId) {
        this.roomTypeId = roomTypeId;
    }

    public int getNoOfRooms() {
        return noOfRooms;
    }

    public void setNoOfRooms(int noOfRooms) {
        this.noOfRooms = noOfRooms;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
