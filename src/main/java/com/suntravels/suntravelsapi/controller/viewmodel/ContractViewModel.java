package com.suntravels.suntravelsapi.controller.viewmodel;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

public class ContractViewModel {

    private UUID id;

    @NotNull
    private UUID hotelId;

    @NotNull
    private Date startDate;

    @NotNull
    private Date endDate;

    @NotNull
    private double markup;




    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getHotelId() {
        return hotelId;
    }

    public void setHotelId(UUID hotelId) {
        this.hotelId = hotelId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public double getMarkup() {
        return markup;
    }

    public void setMarkup(double markup) {
        this.markup = markup;
    }
}
