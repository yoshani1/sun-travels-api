package com.suntravels.suntravelsapi.controller.viewmodel;

import javax.validation.constraints.NotNull;
import java.util.UUID;


public class HotelViewModel {
    private UUID id;

    @NotNull
    private String hotelName;

    private String telephone;

    private String address;


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
