package com.suntravels.suntravelsapi.controller.viewmodel;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class RoomTypeViewModel {

    private UUID id;

    @NotNull
    private UUID hotelId;

    @NotNull
    private String type;

    @NotNull
    private int maxAdults;

    @NotNull
    private int noOfRooms;



    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setRoomTypeId(UUID id) {
        this.id = id;
    }

    public UUID getHotelId() {
        return hotelId;
    }

    public void setHotelId(UUID hotelId) {
        this.hotelId = hotelId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getMaxAdults() {
        return maxAdults;
    }

    public void setMaxAdults(int maxAdults) {
        this.maxAdults = maxAdults;
    }

    public int getNoOfRooms() {
        return noOfRooms;
    }

    public void setNoOfRooms(int noOfRooms) {
        this.noOfRooms = noOfRooms;
    }
}
