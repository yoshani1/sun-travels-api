package com.suntravels.suntravelsapi.controller.viewmodel;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class RoomViewModel {

    private UUID id;

    @NotNull
    private UUID roomTypeId;

    @NotNull
    private boolean availability;



    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getRoomTypeId() {
        return roomTypeId;
    }

    public void setRoomTypeId(UUID roomTypeId) {
        this.roomTypeId = roomTypeId;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }
}
