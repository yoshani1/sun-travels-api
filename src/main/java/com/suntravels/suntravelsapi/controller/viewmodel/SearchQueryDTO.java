package com.suntravels.suntravelsapi.controller.viewmodel;

import java.util.Date;
import java.util.List;

public class SearchQueryDTO {
    private Date startDate;
    private int noOfNights;
    private List<RoomPrefDTO> roomPref;

    public SearchQueryDTO() {
    }

    public SearchQueryDTO(Date startDate, int noOfNights, List<RoomPrefDTO> roomPref) {
        this.startDate = startDate;
        this.noOfNights = noOfNights;
        this.roomPref = roomPref;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getNoOfNights() {
        return noOfNights;
    }

    public void setNoOfNights(int noOfNights) {
        this.noOfNights = noOfNights;
    }

    public List<RoomPrefDTO> getRoomPref() {
        return roomPref;
    }

    public void setRoomPref(List<RoomPrefDTO> roomPref) {
        this.roomPref = roomPref;
    }
}
