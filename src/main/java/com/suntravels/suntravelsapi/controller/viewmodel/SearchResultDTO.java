package com.suntravels.suntravelsapi.controller.viewmodel;

import java.util.List;

public class SearchResultDTO {

    private String hotelName;
    private String hotelAddress;
    private List<SearchResultRoomEntryDTO> searchResultRoomEntryDTOList;

    public SearchResultDTO() {
    }

    public SearchResultDTO(String hotelName, String hotelAddress, List<SearchResultRoomEntryDTO> searchResultRoomEntryDTOList) {
        this.hotelName = hotelName;
        this.hotelAddress = hotelAddress;
        this.searchResultRoomEntryDTOList = searchResultRoomEntryDTOList;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotelAddress() {
        return hotelAddress;
    }

    public void setHotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
    }

    public List<SearchResultRoomEntryDTO> getSearchResultRoomEntryDTOList() {
        return searchResultRoomEntryDTOList;
    }

    public void setSearchResultRoomEntryDTOList(List<SearchResultRoomEntryDTO> searchResultRoomEntryDTOList) {
        this.searchResultRoomEntryDTOList = searchResultRoomEntryDTOList;
    }
}
