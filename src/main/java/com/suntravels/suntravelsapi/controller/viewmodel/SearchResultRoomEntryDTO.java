package com.suntravels.suntravelsapi.controller.viewmodel;

import java.util.List;

public class SearchResultRoomEntryDTO {
    private RoomPrefDTO roomPrefDTO;
    private List<SearchResultTypePriceDTO> searchResultRoomEntryList;

    public SearchResultRoomEntryDTO() {
    }

    public SearchResultRoomEntryDTO(RoomPrefDTO roomPrefDTO, List<SearchResultTypePriceDTO> searchResultRoomEntryList) {
        this.roomPrefDTO = roomPrefDTO;
        this.searchResultRoomEntryList = searchResultRoomEntryList;
    }

    public RoomPrefDTO getRoomPrefDTO() {
        return roomPrefDTO;
    }

    public void setRoomPrefDTO(RoomPrefDTO roomPrefDTO) {
        this.roomPrefDTO = roomPrefDTO;
    }

    public List<SearchResultTypePriceDTO> getSearchResultRoomEntryList() {
        return searchResultRoomEntryList;
    }

    public void setSearchResultRoomEntryList(List<SearchResultTypePriceDTO> searchResultRoomEntryList) {
        this.searchResultRoomEntryList = searchResultRoomEntryList;
    }
}
