package com.suntravels.suntravelsapi.controller.viewmodel;

public class SearchResultTypePriceDTO {

    private String type;
    private double markedUpPrice;

    public SearchResultTypePriceDTO() {
    }

    public SearchResultTypePriceDTO(String type, double markedUpPrice) {
        this.type = type;
        this.markedUpPrice = markedUpPrice;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getMarkedUpPrice() {
        return markedUpPrice;
    }

    public void setMarkedUpPrice(double markedUpPrice) {
        this.markedUpPrice = markedUpPrice;
    }
}
