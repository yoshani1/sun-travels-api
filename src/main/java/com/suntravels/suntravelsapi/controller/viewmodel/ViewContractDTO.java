package com.suntravels.suntravelsapi.controller.viewmodel;

import java.util.Date;
import java.util.List;

public class ViewContractDTO {

    private String hotelName;
    private String hotelAddress;
    private Date startDate;
    private Date endDate;
    private List<ContractRoomEntryDTO> contractRoomEntryDTOList;


    public ViewContractDTO() {
    }

    public ViewContractDTO(String hotelName, String hotelAddress, Date startDate, Date endDate, List<ContractRoomEntryDTO> contractRoomEntryDTOList) {
        this.hotelName = hotelName;
        this.hotelAddress = hotelAddress;
        this.startDate = startDate;
        this.endDate = endDate;
        this.contractRoomEntryDTOList = contractRoomEntryDTOList;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotelAddress() {
        return hotelAddress;
    }

    public void setHotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<ContractRoomEntryDTO> getContractRoomEntryDTOList() {
        return contractRoomEntryDTOList;
    }

    public void setContractRoomEntryDTOList(List<ContractRoomEntryDTO> contractRoomEntryDTOList) {
        this.contractRoomEntryDTOList = contractRoomEntryDTOList;
    }
}
