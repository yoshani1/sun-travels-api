package com.suntravels.suntravelsapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Contract {

    @Id
    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID id = new UUID(0,0);

    private double markup;

    private Date startDate;

    private Date endDate;

    @OneToMany(mappedBy = "contract", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private List<ContractRoomType> contractRoomTypeList;

    @ManyToOne(fetch = FetchType.LAZY)
    private Hotel hotel;

    protected Contract() {
        this.contractRoomTypeList = new ArrayList<>();
    }

    public Contract(int markup, Date startDate, Date endDate, Hotel hotel) {
        this();
        this.markup = markup;
        this.startDate = startDate;
        this.endDate = endDate;
        this.hotel = hotel;
    }

    public Contract(UUID id, int markup, Date startDate, Date endDate, Hotel hotel) {
        this();
        this.id = id;
        this.markup = markup;
        this.startDate = startDate;
        this.endDate = endDate;
        this.hotel = hotel;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public double getMarkup() {
        return markup;
    }

    public void setMarkup(double markup) {
        this.markup = markup;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<ContractRoomType> getContractRoomTypeList() {
        return contractRoomTypeList;
    }

    public void setContractRoomTypeList(List<ContractRoomType> contractRoomTypeList) {
        this.contractRoomTypeList = contractRoomTypeList;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }
}
