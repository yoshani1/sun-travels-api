package com.suntravels.suntravelsapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ContractRoomType {

    @EmbeddedId
    private ContractRoomTypePK contractRoomTypePK;

    private int noOfRooms;

    private double price;

    @MapsId("contractId")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contract_id",  nullable = false, insertable = false, updatable = false)
    private Contract contract;

    @MapsId("roomTypeId")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_type_id",  nullable = false, insertable = false, updatable = false)
    private RoomType roomType;

    public ContractRoomType() {
    }

    public ContractRoomType(int noOfRooms, double price, Contract contract, RoomType roomType) {
        this();
        this.noOfRooms = noOfRooms;
        this.price = price;
        this.contract = contract;
        this.roomType = roomType;
    }

    public ContractRoomType(ContractRoomTypePK contractRoomTypePK, int noOfRooms, double price, Contract contract, RoomType roomType) {
        this();
        this.contractRoomTypePK = contractRoomTypePK;
        this.noOfRooms = noOfRooms;
        this.price = price;
        this.contract = contract;
        this.roomType = roomType;
    }

    public ContractRoomTypePK getContractRoomTypePK() {
        return contractRoomTypePK;
    }

    public void setContractRoomTypePK(ContractRoomTypePK contractRoomTypePK) {
        this.contractRoomTypePK = contractRoomTypePK;
    }

    public int getNoOfRooms() {
        return noOfRooms;
    }

    public void setNoOfRooms(int noOfRooms) {
        this.noOfRooms = noOfRooms;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }
}
