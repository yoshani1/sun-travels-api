package com.suntravels.suntravelsapi.entity;

import org.hibernate.annotations.Type;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class ContractRoomTypePK implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "contract_id")
    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID contractId;

    @Column(name = "room_type_id")
    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID roomTypeId;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContractRoomTypePK that = (ContractRoomTypePK) o;
        return contractId.equals(that.contractId) &&
                roomTypeId.equals(that.roomTypeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contractId, roomTypeId);
    }

    public ContractRoomTypePK() {
    }

    public ContractRoomTypePK(UUID contractId, UUID roomTypeId) {
        this.contractId = contractId;
        this.roomTypeId = roomTypeId;
    }

    public UUID getContractId() {
        return contractId;
    }

    public void setContractId(UUID contractId) {
        this.contractId = contractId;
    }

    public UUID getRoomTypeId() {
        return roomTypeId;
    }

    public void setRoomTypeId(UUID roomTypeId) {
        this.roomTypeId = roomTypeId;
    }
}
