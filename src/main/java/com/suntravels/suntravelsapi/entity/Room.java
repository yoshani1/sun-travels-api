package com.suntravels.suntravelsapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;
import javax.persistence.*;
import java.util.UUID;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Room {

    @Id
    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID id = new UUID(0,0);

    @ManyToOne(fetch = FetchType.LAZY)
    private RoomType roomType;

    private boolean availability;

    protected Room() {
    }

    public Room(RoomType roomType, boolean availability) {
        this();
        this.roomType = roomType;
        this.availability = availability;
    }

    public Room(UUID id, RoomType roomType, boolean availability) {
        this();
        this.id = id;
        this.roomType = roomType;
        this.availability = availability;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }
}
