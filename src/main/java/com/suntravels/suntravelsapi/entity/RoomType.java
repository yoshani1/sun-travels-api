package com.suntravels.suntravelsapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RoomType {

    @Id
    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID id = new UUID(0,0);

    @ManyToOne(fetch = FetchType.LAZY)
    private Hotel hotel;

    private String type;

    private int maxAdults;

    private int noOfRooms;

    @OneToMany(mappedBy = "roomType", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private List<ContractRoomType> contractRoomTypeList;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "roomType", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Room> roomList;

    protected RoomType() {
        this.contractRoomTypeList = new ArrayList<>();
        this.roomList = new ArrayList<>();
    }

    public RoomType(Hotel hotel, String type, int maxAdults, int noOfRooms) {
        this();
        this.hotel = hotel;
        this.type = type;
        this.maxAdults = maxAdults;
        this.noOfRooms = noOfRooms;
    }

    public RoomType(UUID id, Hotel hotel, String type, int maxAdults, int noOfRooms) {
        this();
        this.id = id;
        this.hotel = hotel;
        this.type = type;
        this.maxAdults = maxAdults;
        this.noOfRooms = noOfRooms;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getMaxAdults() {
        return maxAdults;
    }

    public void setMaxAdults(int maxAdults) {
        this.maxAdults = maxAdults;
    }

    public int getNoOfRooms() {
        return noOfRooms;
    }

    public void setNoOfRooms(int noOfRooms) {
        this.noOfRooms = noOfRooms;
    }

    public List<ContractRoomType> getContractRoomTypeList() {
        return contractRoomTypeList;
    }

    public void setContractRoomTypeList(List<ContractRoomType> contractRoomTypeList) {
        this.contractRoomTypeList = contractRoomTypeList;
    }

    public List<Room> getRoomList() {
        return roomList;
    }

    public void setRoomList(List<Room> roomList) {
        this.roomList = roomList;
    }
}
