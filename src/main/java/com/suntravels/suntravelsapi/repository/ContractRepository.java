package com.suntravels.suntravelsapi.repository;

import com.suntravels.suntravelsapi.entity.Contract;
import com.suntravels.suntravelsapi.entity.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public interface ContractRepository extends JpaRepository<Contract, UUID> {

    Contract findByHotelAndEndDateAfterAndStartDateBeforeAndEndDateAfter(Hotel hotel, Date todayDate1, Date todayDate2, Date queryDate);
    List<Contract> findByHotelId(UUID hotelId);
    List<Contract> findAllByOrderByEndDateDesc();

}
