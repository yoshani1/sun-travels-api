package com.suntravels.suntravelsapi.repository;

import com.suntravels.suntravelsapi.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ContractRoomTypeRepository extends JpaRepository<ContractRoomType, ContractRoomTypePK> {
    List<ContractRoomType> findByContractAndRoomTypeMaxAdultsEquals(Contract contract, int noOfAdults);
}
