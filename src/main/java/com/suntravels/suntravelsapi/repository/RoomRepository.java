package com.suntravels.suntravelsapi.repository;

import com.suntravels.suntravelsapi.entity.Room;
import com.suntravels.suntravelsapi.entity.RoomType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.UUID;


@Repository
public interface RoomRepository extends JpaRepository<Room, UUID> {
    List<Room> findByRoomTypeAndAvailabilityTrue(RoomType roomType);
    List<Room> findByRoomType(RoomType roomType);
}
