package com.suntravels.suntravelsapi.repository;

import com.suntravels.suntravelsapi.entity.Hotel;
import com.suntravels.suntravelsapi.entity.RoomType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;


@Repository
public interface RoomTypeRepository extends JpaRepository<RoomType, UUID> {
    List<RoomType> findByHotel(Hotel hotel);
}
